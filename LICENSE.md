<!--
SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# License System

The `vim-config` project uses a dual license system. All source code and
auxiliary data files will be covered by the [ISC license][isc_copy]. All
documentation and artwork will be covered by the
[CC-BY-SA-4.0][cc-by-sa-4.0-copy] license.

Copyright and licensing information is tracked using the [REUSE version
3.0][reuse-3.0-spec] specification. Copyright is validated through usage of the
[Linux Developer Certificate of Origin][linux-dco-spec] specification through
commit history.

__No warranty__ is provided, not even for __merchantability__ or __fitness for
a particular purpose__.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[isc_copy]: https://gitlab.com/dev-dotfile/vim-config/-/blob/main/LICENSES/ISC.txt
[cc-by-sa-4.0-copy]: https://gitlab.com/dev-dotfile/vim-config/-/blob/main/LICENSES/CC-BY-SA-4.0.txt
[reuse-3.0-spec]: https://reuse.software/spec/
[linux-dco-spec]: https://elinux.org/Developer_Certificate_Of_Origin
