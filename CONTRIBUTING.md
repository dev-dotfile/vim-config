<!--
SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Contributing

Thank you for taking the time to contribute to the `vim-config` project.
Always remember that the information stored here is considered general
guidelines __NOT__ hard and fast rules. Therefore, always use your best
judgement!

## Expected Contributions

The `vim-config` project is open to and expects the following forms of
contributions:

- Bug reports.
- Feature requests.
- Bug fixes.
- Feature implementations.
- Improvements to source code.
- Improvements to documentation.
- Improvements to CI/CD pipeline.
- Improvements to build system.

## Reuse Compliance

The `vim-config` project uses the [REUSE Specification 3.0][reuse-spec] to state
what portions of the project are covered by certain licenses in a clear and
concise manner. Therefore, all new files added to this project must add a
valid SPDX license and copyright identifier in order to remain REUSE compliant.
Use the `reuse` tool to figure out if all new files are REUSE compliant like
so:

```
# reuse lint
```

## Coding Style

Keep all lines of source code under 80 characters _if possible_. Indentation is
set to two spaces only. Try your best to provide consistant, readable, and
maintainable code at all times. Thats about it really.

## Commit Style

The `vim-config` project uses [Conventional Commits][conventional-commits] as its
standard commit style. The following are all valid commit types used by the
project:

- __Chore__ - Changes do not apply to source code.
- __Docs__ - Changes apply to documentation.
- __Feat__ - Changes implement a feature.
- __Fix__ - Changes fixes a bug.
- __Perf__ - Changes improve performance.
- __Ref__ - Changes refactor source code.
- __Style__ - Changes fixes to the style of source code or documentation.
- __Revert__ - Changes revert commit history for whatever reason.

Scope is optional, but use camel case, e.g., CoolFeature __NOT__ cool\_feature.
Keep subject, body, and trailer under 72 characters _if possible_. Captialize
the subject line.

__ALL__ commits must have the `Signed-off-by: $NAME $EMAIL` trailer. This is
required, because `vim-config` abides by the [Linux Developer Certificate of
Origin][linux-dco]

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[reuse-spec]: https://reuse.software/spec/
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[linux-dco]: https://developercertificate.org/
