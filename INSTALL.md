<!--
SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Installing

## Software Requirements

- Git [>= 2.20.0].
- Make [Posix compliance at minimum].
- Vim or NeoVim [>= 8.0.0].

Optional, PGP software such as GPG [>= 2.2.1] if you want to verify the
signiture of tagged release versions.

## Getting the Source Code

All source code for the `vim-config` project is hosted over on GitLab at
\<<https://gitlab.com/dev-dotfile/vim-config>\>. A mirror of this repository
exists on GitHub at \<<https://github.com/magikcodec/vim-config>\>.

It is recommended that you clone the project from GitLab, because all
development occurs there. The mirror exists as a simple remote
reference/backup of the project.

If you want up-to-date changes to the codebase, then directly clone the
`main` branch:

```
# git clone --depth 1 https://gitlab.com/dev-dotfile/vim-config.git
```

If you want only a specific tagged release version, then type the following
instead:

```
# git clone --depth 1 --branch $TAG https://gitlab.com/dev-dotfile/vim-config.git
```

_$TAG is the name of the tagged release version, e.g., v1.0.0 or v2.2.1 as tag
names._

You can also just get a copy of the code directly from your browser by visiting
the project on GitLab or GitHub online at the provided URLs.

## Verifying Tagged Releases

All tagged release version of the `vim-config` project are signed using
@magikcodec's GPG key. If you want to verify the signiture of tagged release
versions, then you need to get the public key of @magikcodec. The public key
can be found over at \<<https://keys.openpgp.org/>\>, with the identity of
\<<magikcodec@gmail.com>\>.

Follow the `keys.openpgp.org` [usage guide][key-usage-guide] to get the public
key for your OpenPGP software of choice. Once you have the key verify the
fingerprint by ensuring that it matches the following digits:
`6998 2D8F 8FE6 8486 E5D8  C103 9C02 E782 6CE5 A888` (spaces added for
readability).

Now, change directory to the top-level of your copy of the project and run
the following command:

```
# git verify-tag $TAG
```

_$TAG is the name of the tagged release version, e.g., v1.0.0 or v2.2.1 as tag
names._

Finally, if the fingerprint matches and the git-verify-tag states that the
signiture is valid, then you have a safe copy of the project to use in your
system. However, if the fingerprint __does not__ match or git-verify-tag says
that the signiture __does not__ match, then send an email about this to
@magikcodec at \<<magikcodec@gmail.com>\> __do not__ submit a bug report!

## Installation Process

Change directory to to the top-level of the project and run:

```
# make
# make install
```

You do not need root privilages for a regular user, because the project is
meant to be installed on a user level at the home directory.

The `vim-config` project uses git submodules to manage its plugins for Vim
Pack. Thus, you may need to perform some updates to ensure that everything
was setup correctly like so:

```
# make update
```

The above incantation will update the submodules to the latest version
and modify the commit history automatically.

If you want to initialize the submodules of the project without updating them,
then just run:

```
# make init
```

After the installation, you will find the configuration at `~/.config/vim`.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[key-usage-guide]: https://keys.openpgp.org/about/usage
