" SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
" SPDX-License-Identifier: ISC

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General settings                                                            "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Force VIM to be XDG compliant...
if empty($MYVIMRC) | let $MYVIMRC = expand('<sfile>:p') | endif

if empty($XDG_CACHE_HOME)  | let $XDG_CACHE_HOME  = $HOME."/.cache"       | endif
if empty($XDG_CONFIG_HOME) | let $XDG_CONFIG_HOME = $HOME."/.config"      | endif
if empty($XDG_DATA_HOME)   | let $XDG_DATA_HOME   = $HOME."/.local/share" | endif
if empty($XDG_STATE_HOME)  | let $XDG_STATE_HOME  = $HOME."/.local/state" | endif

set runtimepath^=$XDG_CONFIG_HOME/vim
set runtimepath+=$XDG_DATA_HOME/vim
set runtimepath+=$XDG_CONFIG_HOME/vim/after

set packpath^=$XDG_DATA_HOME/vim,$XDG_CONFIG_HOME/vim
set packpath+=$XDG_CONFIG_HOME/vim/after,$XDG_DATA_HOME/vim/after

let g:netrw_home = $XDG_DATA_HOME."/vim"
call mkdir($XDG_DATA_HOME."/vim/spell", 'p', 0700)

set backupdir=$XDG_STATE_HOME/vim/backup | call mkdir(&backupdir, 'p', 0700)
set directory=$XDG_STATE_HOME/vim/swap   | call mkdir(&directory, 'p', 0700)
set undodir=$XDG_STATE_HOME/vim/undo     | call mkdir(&undodir,   'p', 0700)
set viewdir=$XDG_STATE_HOME/vim/view     | call mkdir(&viewdir,   'p', 0700)

if !has('nvim') " Neovim has its own special location
  set viminfofile=$XDG_STATE_HOME/vim/viminfo
endif

" Enable fileype plugins...
filetype plugin on
filetype indent on

" Set to auto read when a file is modified from the outside...
set autoread
au FocusGained,BufEnter * checktime

" Set history size and cursor lines...
set history=1000
set so=7

" Establish default file settings...
let $LANG = 'en'
set langmenu=en
set encoding=utf8
set ffs=unix,dos,mac
set nobackup
set nowb
set noswapfile

" Set command-line height...
set cmdheight=1

" Enable wild menu...
set wildmenu

" Make backspace more natural...
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Smart searching with casing...
set smartcase

" Highlight searches...
set hlsearch

" More natural searching...
set incsearch

" Do not redraw when executing macros...
set lazyredraw

" Turn on regular expressions...
set magic

" Bracket settings...
set showmatch
set mat=3

" Remove annoying error sounds...
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Enable syntax highlighting...
syntax enable

" Hide abandoned buffers...
set hid

" Specify behavior when switching between buffers...
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Make splits more natural...
set splitbelow
set splitright

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Formatting settings                                                         "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set line break...
set lbr

" Set maximum column limit to 100...
set tw=100

" Use spaces instead of tabs...
set expandtab

" Smart tabbing...
set smarttab

" Enable hard wrap...
set wrap

" 1 tab = 4 spaces...
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Auto indent...
set ai

" Smart indent...
set si

" Return to last edit position when opening files...
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Delete trailing white space on save, useful for some filetypes...
fun! CleanExtraSpaces()
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  silent! %s/\s\+$//e
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfun

if has("autocmd")
  autocmd BufWritePre *.txt,*.md,*.vim,*.c,*.cpp,*.h,*.hpp,*.py,*.sh,*.zsh :call CleanExtraSpaces()
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keyboard mappings                                                           "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Setup leader...
let mapleader = ","

" Normal mode mappings...

" Remap Vim 0 to first non-blank character...
map 0 ^

" Forward search...
map <space> /

" Backwards search...
map <C-space> ?

" Fast save...
nmap <leader>w :w!<cr>

" Fast hard quit...
nmap <leader>q :q!<cr>

" Save as sudo...
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" Window movement...
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Split mappings...
map <leader>hs :split
map <leader>vs :vsplit

" Buffer mappings...
map <leader>bo :enew<cr>
map <leader>bd :Bclose<cr>:tabclose<cr>gT
map <leader>ba :bufdo bd<cr>
map <leader>bl :bnext<cr>
map <leader>bh :bprevious<cr>

" Tab mappings...
map <leader>tn :tabnew
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>th :tabprev<cr>
map <leader>tl :tabnext<cr>
map <leader>te :tabedit <C-r>=expand("%:p:h")<cr>/
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Universal CTags mappings...
map <leader>cts :tselect<cr>
map <leader>ctn :tnext<cr>
map <leader>ctp :tprev<cr>
map <leader>ctf :tfirst<cr>
map <leader>ctl :tlast<cr>

" CScope mappings...
map <leader>csa :cs add cscope.out<cr>
map <leader>css :cs show<cr>
map <leader>csfa :tab cs find a
map <leader>csfc :tab cs find c
map <leader>csfd :tab cs find d
map <leader>csfe :tab cs find e
map <leader>csff :tab cs find f
map <leader>csfg :tab cs find g
map <leader>csfi :tab cs find i
map <leader>csfs :tab cs find s
map <leader>csft :tab cs find t

" Toggle paste mode...
map <leader>pm :setlocal paste!<cr>

" Visual mode mappings...

" Forward search...
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>

" Backwards search...
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Helper for visual selection...
fun! VisualSelection(direction, extra_filter) range
  let l:save_reg = @"
  execute "normal! vgvy"

  let l:pattern = escape(@", "\\/.*'$^~[]")
  let l:pattern = substitute(l:pattern, "\n$", "", "")

  if a:direction == 'gv'
    call CmdLine("Ack '" . l:patern . "' " )
  elseif a:direction == 'replace'
    call CmdLine("%s" . '/'. l:pattern . '/')
  endif

  let @/ = l:pattern
  let @" = l:saved_reg
endfun

" Insert mode mappings...

" Alternate way to escape...
imap jj <Esc>
