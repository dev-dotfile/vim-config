<!--
SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][kac-spec], and this project adheres to
[Semantic Versioning][semver-spec].

## [Unreleased]

## [0.0.0] - 2022-07-13

### Added

- Add ISC to cover source code.
- Dual-license ISC with CC-BY-SA-4.0 to cover documentation and artwork.
- Use `LICENSE.md` to describe dual-license system to users.
- Introduce newcomers to project through `README.md`.
- Provide installation instructions through `INSTALL.md`.
- Give proper contribution guidelines to users via `CONTRIBUTING.md`.
- Setup area to give thanks to contributors and original author via `THANKS.md`
  and `AUTHORS.md`.
- Setup bug report template.
- Setup feature request template.
- Setup standard merge request template.
- Make @magikcodec main code owner.
- Setup textual attributes through `.gitattributes`.

[kac-spec]: https://keepachangelog.com/en/1.0.0/
[semver-spec]: https://semver.org/spec/v2.0.0.html

[Unreleased]: https://gitlab.com/dev-dotfile/vim-config/-/compare/v0.0.0...HEAD
[0.0.0]: https://gitlab.com/dev-dotfile/vim-config/-/tags/v0.0.0
