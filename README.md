<!--
SPDX-FileCopyrightText: 2022 Jason Pena <magikcodec@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Vim Configuration

The `vim-config` project exists as a personal configuration of the Vim and
NeoVim text editors. This project is __not meant__ to be blindly installed as a
general purpose configuration. Instead, it is recommended that you view the
code first, and take or modify the parts you want into your own
configuration with respect to the project's licensing.

## Installation

Clone and run the following commands:

```
# make
# make install
```

Root privilages are not needed, because this configuration targets a single
user's home directory only.

For more detailed instructions regarding the installation of `vim-config`, then
view the [INSTALL.md][install-spec] file.

## Configurations

The [leader][mapleader] is a comma `,`; so, whenever you see `<leader>` it means
to press comma on your keyboard.

### Normal Mode Mappings

| Keys             | Function                                             |
| ---------------- | ---------------------------------------------------- |
| \<space\>        | Search                                               |
| \<C-space\>      | Backwards search                                     |
| \<leader\>\<cr\> | Disable highlights                                   |
| \<leader\>w      | Quick save                                           |
| \<leader\>q      | Quick quit                                           |
| :W               | Save as sudoer                                       |
| \<C-j\>          | Move down a window                                   |
| \<C-k\>          | Move up a window                                     |
| \<C-h\>          | Move left a window                                   |
| \<C-l\>          | Move right a window                                  |
| \<leader\>hs     | Create horizontal split                              |
| \<leader\>vs     | Create vertical split                                |
| \<leader\>bo     | Open new buffer                                      |
| \<leader\>bd     | Close current buffer                                 |
| \<leader\>ba     | Close all buffers                                    |
| \<leader\>bp     | Move to previous buffer                              |
| \<leader\>bn     | Move to next buffer                                  |
| \<leader\>tn     | Open new tab                                         |
| \<leader\>to     | Close all other tabs (keeping current tab open)      |
| \<leader\>th     | Move to previous tab                                 |
| \<leader\>tl     | Move to next tab                                     |
| \<leader\>tm     | Move a tab to a new position                         |
| \<leader\>te     | Open new tab with current buffer's path              |
| \<leader\>cd     | Change directory to the directory of current buffer  |
| \<leader\>cts    | Show CTag list                                       |
| \<leader\>ctn    | Next CTag in list                                    |
| \<leader\>ctp    | Previous CTag in list                                |
| \<leader\>ctf    | Goto first CTag in list                              |
| \<leader\>ctl    | Goto last CTag in list                               |
| \<leader\>csa    | Add CScope database                                  |
| \<leader\>css    | Show loaded CScope database                          |
| \<leader\>csfa   | Use CScope to find assignments to symbol             |
| \<leader\>csfc   | Use CScope to find functions calling this function   |
| \<leader\>csfd   | Use CScope to find functions called by this function |
| \<leader\>csfe   | Use CScope to find this egrep pattern                |
| \<leader\>csff   | Use CScope to find this file                         |
| \<leader\>csfg   | Use CScope to find function definition               |
| \<leader\>csfi   | Find files including this file with CScope           |
| \<leader\>csfs   | Find this C symbol with CScope                       |
| \<leader\>csft   | Find this text string with CScope                    |
| \<leader\>pm     | Toggle paste mode                                    |

### Visual Mode Mappings

| Keys  | Function                           |
| ----- | ---------------------------------- |
| *     | Search current selection           |
| #     | Backwards search current selection |

### Insert Mode Mappings

| Keys  | Function          |
| ----- | ----------------- |
| jj    | Enter normal mode |

## Contributing

This project is open to contribution, and only expects the following:

- Bug reports.
- Feature requests.
- Bug fixes.
- Feature implementations.
- Improvements to source code.
- Improvements to documentation.
- Improvements to CI/CD pipeline(s).
- Improvements to build system.

Please view the [contribution guidelines][contribution-spec] for more detailed
information regarding proper contribution to the `vim-config` project.

## Licensing

The `vim-config` project uses a dual license system with the ISC and
CC-BY-SA-4.0 licenses. For more information regarding this licensing system
view the [LICENSE.md][license-spec] file.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[install-spec]: https://gitlab.com/dev-dotfile/vim-config/-/blob/main/INSTALL.md
[contribution-spec]: https://gitlab.com/dev-dotfile/vim-config/-/blob/main/CONTRIBUTING.md
[license-spec]: https://gitlab.com/dev-dotfile/vim-config/-/blob/main/LICENSE.md
